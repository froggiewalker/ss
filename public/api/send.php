<?php

define("EMAIL_REGEXP", '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD');

try{

    if (!$_POST['data']) {
        throw new Exception('No input data');
    }

    $podzimnicci = $_POST['data'];

    if (gettype($podzimnicci) !== "array") {
        throw new Exception('data not array');
    }

    // validate
    foreach ($podzimnicci as $key => $podzimnicek) {
        $name = $podzimnicek['name'];
        $mail = $podzimnicek['mail'];
        if (!$name || gettype($name) !== 'string' || !strlen($name)) {
            throw new Exception("Missing or wrong name attribute of $key: '$name'");
        }
        if (!$mail || gettype($mail) !== 'string' || !preg_match(EMAIL_REGEXP, $mail)) {
            throw new Exception("Missing or wrong mail attribute of $key: '$mail'");
        }
    }
    $count = count($podzimnicci);
    if ($count < 3) {
        throw new Exception("It doesn't make sense to draw only $count podzimnicky.");
    }
    $emailTemplate = $_POST['email_template'];
    if (!$emailTemplate || gettype($emailTemplate) !== 'string' || !strlen($emailTemplate)) {
        throw new Exception("Invalid message template");
    }

    // shuffle and check if someone gives himself
    $podzimnicci = draw($podzimnicci);

    // send emails
    foreach ($podzimnicci as $podzimnicek) {
        sendMail($podzimnicek['name'], $podzimnicek['mail'], $podzimnicek['victim']['name'], $podzimnicek['victim']['mail'], $emailTemplate);
    }


} catch (Exception $e) {
    die("Error: {$e->getMessage()}\n<br><a href='/'>go back</a>");
}

header('Location: /');




function draw($podzimnicci) {
    // create shuffled array
    $shuffled = $podzimnicci;
    shuffle($shuffled);

    $extended = $podzimnicci;
    foreach ($shuffled as $key => $podzimnicek) {
        if ($podzimnicek['id'] != $key) {
            $extended[$key]['victim'] = $podzimnicek;
        } else {
            return draw($podzimnicci);
        }
    }
    return $extended;
}



/**
 * Composes and sends email
 */
function sendMail($giveName, $giveMail, $getName, $getMail, $emailTemplate) {
    $subject = 'Ivácký Vánoční Automat';
    $placeholders = array('[[give_name]]', '[[give_mail]]', '[[get_name]]', '[[get_mail]]');
    $values = array($giveName, $giveMail, $getName, $getMail);

    $message = str_replace($placeholders, $values, $emailTemplate);
    $message = str_replace("\n", "<br>", $message);

    $headers = 'From: ss@migdal.cz' . "\r\n" .
        'Reply-To: jonas.jonas.jonas@gmail.com' . "\r\n" .
        'Content-Type: text/html; charset="UTF-8"' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    $sent = mail($giveMail, $subject, $message, $headers);

    if (!$sent) {
        throw new Exception('Email not sent. Error: ' . error_get_last()['message']);
    }
}


?>
